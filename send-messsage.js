const { io } = require("socket.io-client");
const axios = require("axios");

const API_ENDPOINT = "http://120.138.72.45:11151/api";
// const API_ENDPOINT = "http://localhost:5000/api";
const GROUP_PREFIX = "ahuhu";
const GROUP_NUM = 1000;
const BATCH_SIZE = 20;

const client = axios.create({ baseURL: API_ENDPOINT });
client.defaults.timeout = 100;
// const token =
//   "aWQ9MTI1Mjk3NzU0MCZ1c2VybmFtZT1kaWVudG10JnNvY2lhbD16YWNjJnNvY2lhbG5hbWU9ZGllbnRtdCZhdmF0YXI9aHR0cCUzQSUyRiUyRnppbmdwbGF5LnN0YXRpYy5nNi56aW5nLnZuJTJGaW1hZ2VzJTJGenBwJTJGenBkZWZhdWx0LnBuZyZ0aW1lPTE2Njc4Nzg1NDImb3RoZXI9ZGVmYXVsdCUzQSUzQSUzQSUzQTEyNTI5Nzc1NDAlM0ElM0ExMDAmdG9rZW5LZXk9M2E2ZGYyMTAyNTE5YjQzMWUwYzNhNDZiZGQ4NWY0NzM=";
// const config = {
//   headers: { Authorization: `Bearer ${token}` },
// };

const main = async () => {
  const a = [];
  const batchs = [];
  for (let i = 0; i < GROUP_NUM; ++i) a.push(i);
  while (a.length > 0) {
    batchs.push(a.splice(0, BATCH_SIZE))
  }

  while (true) {
    for (let index = 0; index < batchs.length; ++index) {
      let cnt = 0;
      await Promise.all(
        batchs[index].map(async (i) => {
          const group = GROUP_PREFIX + i;
          await client
            .post(
              "/v1/channels/" + group + "/message",
              {
                message: "" + Date.now(),
              },
              config
            )
            .then((data) => {
              cnt++;
            })
            .catch(() => {});
        })
      );
      console.log({ cnt });
    }
  }
};

main();
