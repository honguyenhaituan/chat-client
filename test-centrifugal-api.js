import http from 'k6/http';

export const options = {
  scenarios: {
    constant_request_rate: {
      executor: 'constant-arrival-rate',
      rate: 3015,
      timeUnit: '1s', // 1000 iterations per second, i.e. 1000 RPS
      duration: '60s',
      preAllocatedVUs: 1000, // how large the initial pool of VUs would be
      maxVUs: 2000, // if the preAllocatedVUs are not enough, we can initialize more
    },
  },
};

export default function () {
  const MAX_GROUP = 20000;
  const group = Math.floor(Math.random() * MAX_GROUP)
  const url = 'http://120.138.72.45:11143/api';
  const payload = JSON.stringify({
    method: "publish",  
    params: {
        channel: "ahuhu" + group, 
        data: {
            time: Date.now()
        }
    }
  });

  const params = {
    headers: {
      'Content-Type': 'application/jsn',
       Authorization: `apikey my_api_key`,
    },
  };

  http.post(url, payload, params);
}