const { Centrifuge } = require("centrifuge");
global.WebSocket = require("ws");

var jwt = require("jsonwebtoken");

const GROUP_PREFIX = "ahoho";
const GROUP_MEMBER = 2;
const GROUP_NUM = 1000;
const BATCH = 10;
let times = [];
let totalConnect = 0;
let message = 0;
const subs = [];

const createClient = (uid, group) => {
  var token = jwt.sign(
    {
      sub: uid,
      exp: 1755448299,
    },
    "my_secret",
    { algorithm: "HS256" }
  );

  var centrifuge = new Centrifuge("ws://120.138.72.45:11146/connection/websocket", {
    token,
  });
  centrifuge.on("connected", () => {
    totalConnect++;
  });
  centrifuge.on("error", (data) => {
    console.log(data);
    totalConnect--;
  })
  centrifuge.connect();

  const sub = centrifuge.newSubscription(group);
  sub.on("publication", function (ctx) {
    times.push(Date.now() - ctx.data.time);
    message++;
  });
  sub.on("unsubscribed", function (ctx) {
    console.log({ ctx });
  });
  sub.subscribe();
  subs.push(sub);
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const main = async () => {
  for (let index = 0; index < GROUP_MEMBER * GROUP_NUM; ++index) {
    const currentGroup = Math.floor(index / GROUP_MEMBER);
    const group = GROUP_PREFIX + currentGroup;
    createClient("" + index, group);
    await sleep(2);
  }

  setInterval(() => {
    for (let i = 0; i < BATCH; ++i) {
      const index = Math.floor(Math.random() * GROUP_NUM * GROUP_MEMBER);
      subs[index].publish({time: Date.now()}).catch((err) => console.log(err))
    }
  }, 20)
}

main()

setInterval(() => {
  if (times.length == 0) {
    console.log({ totalConnect });
    return;
  }

  times.sort(function (a, b) {
    return a - b;
  });
  console.log({
    totalConnect, 
    total: times.length,
    message,
    min: times[0],
    max: times[times.length - 1],
    mean: times.reduce((a, b) => (a + b, 0)) / times.length,
    p50: times[times.length >> 1],
    p95: times[Math.floor((times.length * 95) / 100)],
    p99: times[Math.floor((times.length * 99) / 100)],
  });
  message = 0;
}, 1000);
