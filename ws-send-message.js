const { io } = require("socket.io-client");
const axios = require("axios");

const SOCKET_ENDPOINT = "http://120.138.72.45:11175/chat";

const GROUP_PREFIX = "ahuhu";
const GROUP_NUM = 10;

const socket = io(SOCKET_ENDPOINT, {
  transports: ["websocket"],
  forceNew: true,
});

socket.on("connect", () => {
  console.log("connect");
  let i = 0;
  setInterval(() => {
    socket.emit("sendMessage", "chat:ahuhu" + (i % GROUP_NUM), Date.now());
    i++;
  }, 1);
});

socket.on("disconnect", () => {
  console.log("disconnect");
});
