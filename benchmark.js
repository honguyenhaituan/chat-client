var io = require("socket.io-client");

var message = "o bispo de constantinopla nao quer se desconstantinopolizar";

function user(shouldBroadcast) {
  var socket = io.connect("http://localhost:5101/ZPS_NAMESPACE", {
    transports: ["websocket"],
    forceNew: true,
  });

  socket.on("connect", function () {
    // Start messaging loop
    if (shouldBroadcast) {
      // message will be broadcasted by server
      socket.emit("broadcast", message);
    } else {
      // message will be echoed by server
      socket.send(message);
    }

    socket.on("message", function (message) {
      socket.send(message);
    });

    socket.on("broadcastOk", function () {
      socket.emit("broadcast", message);
    });
  });
}

var argvIndex = 2;

var users = parseInt(process.argv[argvIndex++]);
var rampUpTime = parseInt(process.argv[argvIndex++]) * 1000; // in seconds
var newUserTimeout = rampUpTime / users;
var shouldBroadcast = true;
var host = process.argv[argvIndex++]
  ? process.argv[argvIndex - 1]
  : "localhost";
var port = process.argv[argvIndex++] ? process.argv[argvIndex - 1] : "3000";

for (var i = 0; i < users; i++) {
  setTimeout(function () {
    user(shouldBroadcast);
  }, i * newUserTimeout);
}
