import http from 'k6/http';

export default function () {
  const MAX_GROUP = 1000;
  const group = Math.floor(Math.random() * MAX_GROUP)
  const url = 'http://120.138.72.45:11159/api/v1/channels/ahuhu' + group + '/message';
  const payload = JSON.stringify({
    message: "" + Date.now()
  });

  const token =
    "aWQ9MTI1Mjk3NzU0MCZ1c2VybmFtZT1kaWVudG10JnNvY2lhbD16YWNjJnNvY2lhbG5hbWU9ZGllbnRtdCZhdmF0YXI9aHR0cCUzQSUyRiUyRnppbmdwbGF5LnN0YXRpYy5nNi56aW5nLnZuJTJGaW1hZ2VzJTJGenBwJTJGenBkZWZhdWx0LnBuZyZ0aW1lPTE2Njc4Nzg1NDImb3RoZXI9ZGVmYXVsdCUzQSUzQSUzQSUzQTEyNTI5Nzc1NDAlM0ElM0ExMDAmdG9rZW5LZXk9M2E2ZGYyMTAyNTE5YjQzMWUwYzNhNDZiZGQ4NWY0NzM=";

  const params = {
    headers: {
        'Content-Type': 'application/json',
       Authorization: `Bearer ${token}`,
    },
  };

  http.post(url, payload, params);
}