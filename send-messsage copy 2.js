const { io } = require("socket.io-client");
const axios = require("axios");

const API_ENDPOINT = "http://120.138.72.22:10150/api";
// const API_ENDPOINT = "http://localhost:4999/api";
const GROUP_PREFIX = "ahuhu";
const GROUP_NUM = 500;

const client = axios.create({ baseURL: API_ENDPOINT });

const main = async () => {
  while (true) {
    for (let i = 0; i < GROUP_NUM; i++) {
      const group = GROUP_PREFIX + i;
      await client
        .post("/v1/channels/" + group + "/message", {
          message: "" + Date.now(),
        })
        .then((data) => {
          console.log({ i });
        });
    }
  }
};

main();
