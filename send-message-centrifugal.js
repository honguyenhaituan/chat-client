const { Centrifuge } = require("centrifuge");
global.WebSocket = require("ws");

var jwt = require("jsonwebtoken");

const GROUP_PREFIX = "ahuhu";
const GROUP_NUM = 1000;
const BATCH = 1;
let totalConnect = 0;

var token = jwt.sign(
  {
    sub: "0",
    exp: 1755448299,
  },
  "my_secret",
  { algorithm: "HS256" }
);

var centrifuge = new Centrifuge("ws://120.138.72.45:11143/connection/websocket", {
  token,
});
centrifuge.on("connected", () => {
  totalConnect++;
});
centrifuge.on("error", (data) => {
  console.log(data);
  totalConnect--;
})
centrifuge.on("publication", (data) => {
  console.log({"pub": data});
})
centrifuge.connect();

const subs = [];
for (let i = 0; i < GROUP_NUM; ++i) {
  const sub = centrifuge.newSubscription(GROUP_PREFIX + i); 
  // sub.on('publication', (data) => {console.log(data)})
  sub.on('error', (data) => {
    console.log(data)
  })
  sub.subscribe(); 
  subs.push(sub)
}

let index = 0;
setInterval(() => {
  for (let i = 0; i < BATCH; i++) {
    subs[index % GROUP_NUM].publish({time: Date.now()}).catch((err) => console.log(err))
    index++;
  }
}, 1);