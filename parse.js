const fs = require('fs')

let data = fs.readFileSync('a.txt').toString();

data = data.replaceAll("}", "}, ")
        .replaceAll("totalConnect", `"totalConnect"`)
        .replaceAll("total:", `"total":`)
        .replaceAll("min", `"min"`)
        .replaceAll("max", `"max"`)
        .replaceAll("p50", `"p50"`)
        .replaceAll("p95", `"p95"`)
        .replaceAll("p99", `"p99"`)
        .replaceAll("mean", `"mean"`)
        .trim()
        .slice(0, -1)

const arr = JSON.parse(`[${data}]`)

let totalConnect = 0; 
let total = 0; 
const p95 = [];
const p99 = []

for (let i = 0; i < arr.length; ++i) {
  totalConnect += arr[i].totalConnect;
  total += arr[i].total;
  p95.push(arr[i].p95)
  p99.push(arr[i].p99)
}

p95.sort((a, b) => a - b)
p99.sort((a, b) => a - b)

console.log({totalConnect: totalConnect / arr.length, total: total/arr.length,p99: p99[Math.floor((p99.length * 75) / 100)],p95: p95[Math.floor((p95.length * 75) / 100)]})
