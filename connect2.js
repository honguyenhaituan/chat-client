const { io } = require("socket.io-client");
const axios = require("axios");

const SOCKET_ENDPOINTS = [
  "http://120.138.72.45:11175/chat",
];

const GROUP_PREFIX = "ahuhu2";
const GROUP_MEMBER = 4;
const GROUP_NUM = 10;
const TIME_CONNECT = 2000;
const BATCH = 1;
let times = [];
let totalConnect = 0;
let message = 0;
const sockets = [];
const channels = [];
const flag = 0;

const createClient = (group, endpoint) => {
  flag = 1;
  const socket = io(endpoint, {
    transports: ["websocket"],
    forceNew: true,
  });

  socket.on("connect", () => {
    flag = 0;
    totalConnect++;
    socket.emit("joinChannel", "chat:" + group);
  });
  socket.on("new-message", (data) => {
    times.push(Date.now() - parseInt(data));
    message++;
  });
  socket.on("disconnect", () => {
    totalConnect--;
  });

  sockets.push(socket); 
  channels.push("chat:" + group)
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const main = async () => {
  for (let index = 0; index < GROUP_NUM * GROUP_MEMBER;) {
    if (flag == 0) {
      const currentGroup = Math.floor(index / GROUP_MEMBER);
      const group = GROUP_PREFIX + currentGroup;
      createClient(
        group,
        SOCKET_ENDPOINTS[index % SOCKET_ENDPOINTS.length]
      );
      index++;
    }
    await sleep(Math.max(TIME_CONNECT / (GROUP_NUM * GROUP_MEMBER), 1))
  }

  setInterval(() => {
    for (let i = 0; i < BATCH; ++i) {
      const index = Math.floor(Math.random() * GROUP_NUM * GROUP_MEMBER);
      sockets[index].emit("sendMessage", channels[index], Date.now());
    }
  }, Math.floor(Math.random() * 200))
}

main()

setInterval(() => {
  if (times.length == 0) {
    console.log({ totalConnect });
    return;
  }

  times.sort(function (a, b) {
    return a - b;
  });
  console.log({
    totalConnect,
    total: times.length,
    "message/s": message,
    min: times[0],
    max: times[times.length - 1],
    mean: times.reduce((a, b) => (a + b, 0)) / times.length,
    p50: times[times.length >> 1],
    p95: times[Math.floor((times.length * 95) / 100)],
    p99: times[Math.floor((times.length * 99) / 100)],
  });
  times.length = 0;
  message = 0;
}, 1000);